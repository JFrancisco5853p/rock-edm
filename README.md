# rock-edm

appRock&Festival
Rock and EDM Festival

## Test and Deploy

To do a deploy you will need to load the files, since the others are only used to perform the gulp tasks that are described in the gulp.file.js file, doing these tasks loads everything in the build folder.
```
-build
- video
-index.html
```
## Name
Rock and EDM Festival

## Description
The project is focused on showing a web page advertising a music event, indicating the date and cost of ticket sales, as well as the place where it will take place, the different times that will be held are also shown.

This project has different gulp development dependencies, to perform various tasks, such as converting .jpg, .png images into less heavy files such as .webp, .avif and thus improve the performance of the page, these tasks are executed in the file " gulp.file.js".

## Installation
Load the project in a local environment, do the following:
```
install npm
npm run dev
```
ready...

## Usage
invitation for rock event

## Support
franciscosobrevilla97@gmail.com

## Authors and acknowledgment
Juan Francisco Sobrevilla Salvador

## License
None

## Project status
None
