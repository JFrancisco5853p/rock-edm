//"src": Obtenido de gulp, para identificar un archivo o una serie de archivos, es el source o una fuente.
//"dest": Permitira almacenar algo en una carpeta destino.
const { src, dest, watch, parallel } = require("gulp")

//CSS
const sass = require("gulp-sass")(require("sass"));
const plumber = require('gulp-plumber');
const autoprefixer = require('autoprefixer');//Mejora css en cualquier navegador
const cssnano = require('cssnano');//Comprimir el codigo css
const postcss = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');

//Imagenes
const cache = require('gulp-cache');
const imagemin = require('gulp-imagemin');
const webp = require('gulp-webp');
const avif = require('gulp-avif');

//JavaScript
const terser = require('gulp-terser-js');

function css(done)
{
    src("src/scss/**/*.scss")//Identificar el archivo de SASS, compilamos con la funcion 
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sass())//Compilarlo
    .pipe(postcss([ autoprefixer(), cssnano()]) )
    .pipe(sourcemaps.write('.'))
    .pipe(dest("build/css"));//Almacenarlo en el disco duro
    
    done();//Callback que revisa a gulp cuando llegamos al final
}

function imagenes(done) {
    //Se optimizan las imagenes png y jpg
    const opciones = {
        optimizationLevel: 3
    }
    src('img/**/*.{png,jpg}')
        .pipe(cache( imagemin(opciones) ))
        .pipe( dest('build/img'));

    done();
}

function versionWebp(done) {
    //Conversion de imagenes a webp
    const opciones = {
        quality: 50
    };

    src("img/**/*.{png,jpg}")
        .pipe(webp(opciones))
        .pipe(dest("build/img"));

    done();
}

function versionAvif(done){
    //Conversion de imagenes a avif
    const opciones = {
        quality: 50
    };

    src('img/**/*.{png,jpg}')
        .pipe(avif(opciones))
        .pipe(dest('build/img'));
    
    done();
}

function javascript(done) {
    src('src/js/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(terser())
    .pipe(sourcemaps.write('.'))
    .pipe(dest('build/js'));
    
    done();
}

function dev(done){
    watch("src/scss/**/*.scss", css);//Para escuchar a todos los archivos
    watch("src/js/**/*.js", javascript);
    done();
}

exports.css = css;
exports.js = javascript;
exports.imagenes = imagenes;
exports.versionWebp = versionWebp;
exports.versionAvif = versionAvif;
exports.dev = parallel(imagenes, versionWebp, versionAvif, javascript, css, dev);//Se ejecuta mas de una funcion en paralelo